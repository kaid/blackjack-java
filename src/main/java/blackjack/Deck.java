package blackjack;

import java.util.Collections;
import java.util.List;

public class Deck {
    final protected List<Card> cards;

    public Deck() {
        cards = Card.all();
    }

    public Card drawBy(Participant participant) {
        return drawBy(participant, true);
    }

    public Card drawBy(Participant participant, boolean faceDown) {
        Card card = cards.remove(cards.size() - 1);
        card.faceDown = faceDown;
        participant.hand.add(card);
        return card;
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }
}
