package blackjack;

public enum Suit {
    HEARTS("♥"),
    DIAMONDS("♦"),
    CLUB("♣"),
    SPADES("♠");

    private String string;

    private Suit(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
