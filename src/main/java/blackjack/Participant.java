package blackjack;

import java.util.ArrayList;
import java.util.List;

abstract public class Participant {
    final public List<Card> hand;
    final public Blackjack game;
    final public Score score;
    protected Boolean stand;

    public Participant(Blackjack game) {
        this.hand  = new ArrayList<Card>();
        this.game  = game;
        this.stand = false;
        this.score = new Score();
    }

    abstract void showHand();
    abstract void play();

    public Boolean is21() {
        return score.is21();
    }

    public Boolean isBlackjack() {
        return is21() && hand.size() == 2;
    }

    public void chooseHit() {
        dealt();
    }

    public void chooseStand() {
        this.stand = true;
    }

    public Boolean isStand() {
        return this.stand;
    }

    public Boolean isBusted() {
        return score.getLower() > 21;
    }


    protected void dealt() {
        score.addFromCard(game.deck.drawBy(this));
    }

    protected void dealt(Boolean faceDown) {
        score.addFromCard(game.deck.drawBy(this, faceDown));
    }

    public void reset() {
        stand = false;
        score.reset();
        while (hand.size() > 0) game.discardPile.add(hand.remove(0));
    }
}
