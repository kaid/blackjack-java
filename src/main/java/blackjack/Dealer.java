package blackjack;


public class Dealer extends Participant {
    public Dealer(Blackjack game) {
        super(game);
    }

    @Override
    public void showHand() {
        System.out.println("\n::==== Dealer currently have:");
        for (Card card : hand) {
            System.out.printf(":-- %s\n", (card.faceDown && game.running) ? "FACE DOWN" :card);
        }
    }

    @Override
    public void play() {
        if (score.validMax() > 0 && score.validMax() < 17) {
            System.out.println("\n::==== Dealer hits.");
            chooseHit();
        } else {
            System.out.print("\n::==== Dealer stands.");
            chooseStand();
        }
    }

    @Override
    public String toString() {
        return "dealer";
    }
}
