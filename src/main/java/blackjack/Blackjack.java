package blackjack;

import java.util.ArrayList;
import java.util.List;

public class Blackjack {
    final protected Deck deck;
    final protected Dealer dealer;
    final protected Player player;
    final protected List<Card> discardPile;
    private Participant winner;
    protected Boolean running;
    protected Integer playerBet;

    public Blackjack() {
        deck   = new Deck();
        dealer = new Dealer(this);
        player = new Player(this);

        playerBet   = 0;
        discardPile = new ArrayList<Card>();
    }

    public void setup() {
        this.running = true;

        System.out.println("::==== A new run started! Getting your initial hand.");
        System.out.printf("::==== Deck: %s cards, Discard pile: %s cards\n", deck.cards.size(), discardPile.size());

        // Let player place her/his bet
        System.out.printf("\n::==== You currently have %s chips, place you bet for this run: ", player.chips);
        player.bet();

        for (int i = 0; i < 2; i++) {
            player.dealt(i == 1);
            dealer.dealt(i == 1);
        }

        player.showHand();
        dealer.showHand();
    }

    public void run() throws InterruptedException {
        Thread.sleep(2000);
        setup();

        while(running) new Round(this).run();

        announce();

        if (player.chips == 0 && winner == dealer) {
            System.out.println("\n::==== Uh oh! Seems you have ran out of your chips, seek your luck next time.");
            quit();
        }

        Thread.sleep(2000);
        tearDown();

        if (!running) run();
    }

    public void quit() {
        System.exit(0);
    }

    private void tearDown() {
        this.winner = null;

        dealer.reset();
        player.reset();

        if (deck.cards.size() < 8) {
            System.out.println("::==== Putting discard pile back into deck....");

            for (Card card : discardPile) {
                deck.cards.add(card);
            }

            discardPile.clear();
        }

        deck.shuffle();
    }

    public void setWinner(Participant winner) {
        this.winner = winner;
        if (winner == player) player.chips += 2 * playerBet;
        playerBet = 0;
        this.running = false;
    }

    public Boolean isAllStand() {
        return dealer.isStand() && player.isStand();
    }

    public void standOff() {
        player.chips += playerBet;
        playerBet = 0;
    }

    private void announce() {
        dealer.showHand();
        player.showHand();

        String content = winner == null ? "    Standoff!   " : String.format("Winner is %s!", winner);
        System.out.println("\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.printf("$======= %s =======$\n", content);
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n\n");
    }

    public void decideWinner() {
        switch (player.score.validMax().compareTo(dealer.score.validMax())) {
            case 1:
                setWinner(player);
                break;
            case 0:
                standOff();
                break;
            case -1:
                setWinner(dealer);
                break;
        }
        this.running = false;
    }
}
