package blackjack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Rank {
    ACE("A", 1, 11),
    TWO("2", 2),
    THREE("3", 3),
    FOUR("4", 4),
    FIVE("5", 5),
    SIX("6", 6),
    SEVEN("7", 7),
    EIGHT("8", 8),
    NINE("9", 9),
    TEN("10", 10),
    JACK("J", 10),
    QUEEN("Q", 10),
    KING("K", 10);


    private List<Integer> values;
    private String string;

    private Rank(String string, Integer... values) {
        this.string = string;
        this.values = Arrays.asList(values);
        Collections.sort(this.values);
    }

    public Integer max() {
        return Collections.max(this.values);
    }

    public Integer min() {
        return Collections.min(this.values);
    }

    @Override
    public String toString() {
        return string;
    }
}
