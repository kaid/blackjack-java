package blackjack;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Player extends Participant {
    private Scanner scanner;
    protected Integer chips;

    public Player(Blackjack game) {
        super(game);
        chips   = 100;
        scanner = new Scanner(System.in);
    }

    public void bet() {
        try {
            Integer betAmount = scanner.nextInt();

            if (betAmount < 1 || betAmount > chips) {
                invalidBetRetry();
                return;
            }

            chips -= betAmount;
            game.playerBet += betAmount;
            System.out.printf("::==== You bet %s chips.\n", betAmount);
        } catch (InputMismatchException e) {
            scanner.next();
            invalidBetRetry();
        }
    }

    private void invalidBetRetry() {
        System.out.printf("::==== Not a valid bet, you should bet at least 1 chip or at most %d chip(s).", chips);
        System.out.print("::==== Please place your bet agein: ");
        bet();
    }

    @Override
    public void showHand() {
        System.out.println("\n::==== You currently have:");

        for (Card card : hand) {
            System.out.printf(":-- %s\n", card);
        }

        System.out.printf("::==== Your score is %d / %d\n", score.getHigher(), score.getLower());
    }

    @Override
    public void play() {
        Character playerChoice = scanner.next().charAt(0);
        switch (playerChoice) {
            case 'h':
                this.chooseHit();
                break;
            case 's':
                this.chooseStand();
                break;
            case 'q':
                System.out.println("::==== Good game, bye bye!");
                game.quit();
            default:
                System.out.print("::==== Not a valid choice, please choose between 'h' and 's', otherwise, choose 'e' to quit: ");
                play();
        }
    }

    @Override
    public String toString() {
        return "player";
    }
}
