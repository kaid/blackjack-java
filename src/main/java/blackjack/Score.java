package blackjack;


public class Score {
    private Integer higher;
    private Integer lower;

    public Score() {
        this.higher = 0;
        this.lower  = 0;
    }

    public Score addFromCard(Card card) {
        lower  += card.rank.min();
        higher += card.rank.max();
        return this;
    }

    public Integer getHigher() {
        return higher;
    }

    public Integer getLower() {
        return lower;
    }

    public Boolean is21() {
        return higher == 21 || lower == 21;
    }

    public Integer validMax() {
        if (higher <= 21) return higher;
        if (higher > 21 && lower <= 21) return lower;
        return -1;
    }

    public void reset() {
        higher = 0;
        lower = 0;
    }
}
