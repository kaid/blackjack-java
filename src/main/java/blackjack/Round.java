package blackjack;


public class Round {
    private Blackjack game;

    public Round(Blackjack game) {
        this.game = game;
    }

    public void run() {

        // Check whether dealer or player get a blackjack
        if (game.dealer.isBlackjack()) {
            System.out.println("\n::==== Dealer blackjack!");
            game.setWinner(game.dealer);
            return;
        }

        if (game.player.isBlackjack()) {
            System.out.printf("\n::==== Player blackjack!");
            game.setWinner(game.player);
            return;
        }

        // Check whether dealer or player is busted
        if (game.dealer.isBusted()) {
            System.out.println("\n::==== Dealer busted!");
            game.setWinner(game.player);
            return;
        }

        if (game.player.isBusted()) {
            System.out.println("\n::==== Player busted!");
            game.setWinner(game.dealer);
            return;
        }

        // When dealer and player both chose stand, compares scores and decides the winner
        if (game.isAllStand()) {
            game.decideWinner();
            return;
        }

        // If player haven't chosen stand, print choices for her/him
        if (!game.player.isStand()) {
            System.out.println("\n::==== Do you want to hit or stand?");
            System.out.println(":-- (h)hit");
            System.out.println(":-- (s)stand");
            System.out.println(":-- (q)quit");
            System.out.print("::==== You choose (h/s): ");
        }

        // If none of the above matches, continue current run
        game.player.play();
        game.player.showHand();

        game.dealer.play();
        game.dealer.showHand();
    }

}
