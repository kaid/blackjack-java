package blackjack;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Card {
    final public Rank rank;
    private Suit suit;
    public Boolean faceDown = false;

    static public List<Card> all() {
        List<Card> cards = new ArrayList<Card>();

        for (Rank rank : Rank.values()) {
            for (Suit suit : Suit.values()) {
                cards.add(new Card(rank, suit));
            }
        }

        Collections.shuffle(cards);

        return cards;
    }

    private Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return rank + " " + suit;
    }
}
